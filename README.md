# chclLCD

Emulates a Matrix Orbital LK-series display. Supports most commands, even some undocumented ones.

## Building

1. Edit `include/Configuration.h` to match your display and board wiring. Default is for a 40x2 display, with the board design I've sent out to some people.
2. Build with platform.io and flash an Arduino Nano.

## License

The board design and firmware is released under the [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2) license (unless otherwise noted). This is basically (in a non-legally-binding way) the equivalent of the GPL for hardware. **This was done to avoid commercial exploitation.** You **cannot** integrate this into a product without also releasing that product under the same license. At the link above there is a nice user guide telling you what you can and can't do, and what obligations you have.

If you don't like it, go give Matrix Orbital money instead. They seem like nice people.

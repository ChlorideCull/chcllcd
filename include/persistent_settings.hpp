/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#ifndef PERSISTENT_SETTINGS_HPP
#define PERSISTENT_SETTINGS_HPP
#include <stdint.h>
#include "Configuration.h"

struct __attribute__ ((packed)) persistent_settings
{
    uint8_t version;
    uint8_t identity;
    uint32_t baudRate;
    uint8_t bootScreen[CONF_LCD_ROWS][CONF_LCD_COLS];
    uint8_t customCharacterBanks[5][8][8];
};

const int BANKID_STARTUP = 0;
const int BANKID_HORIZONTAL_BARS = 1;
const int BANKID_VERTICAL_BARS = 2;
const int BANKID_MEDIUM_DIGITS = 3;
const int BANKID_LARGE_DIGITS = 4;

void clear_persistent_settings();
void get_persistent_settings(persistent_settings&);
void set_persistent_settings(const persistent_settings&);


#endif
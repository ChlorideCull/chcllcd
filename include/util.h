/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#ifndef UTIL_H
#define UTIL_H
#include <Arduino.h>
inline uint8_t blockingSerialRead() {
    while (Serial.available() == 0) {}
    return Serial.read();
};

#endif
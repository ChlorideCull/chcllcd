/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#ifndef LCD_FRAMEBUFFER_HPP
#define LCD_FRAMEBUFFER_HPP
#include <LiquidCrystal.h>

class lcd_framebuffer
{
private:
    uint8_t cursor_col;
    uint8_t cursor_row;
    uint8_t columns;
    uint8_t rows;
    uint8_t framebuffer[4][40];
    LiquidCrystal* lcd;

    void scroll();
public:
    bool autoScroll = true;
    bool lineWrap = true;
    lcd_framebuffer(LiquidCrystal& lcd, uint8_t columns, uint8_t rows);
    ~lcd_framebuffer();
    void write(uint8_t);
    void write(const char*);
    void sync();
    void setCursor(uint8_t col, uint8_t row);
    void clear();
};


#endif
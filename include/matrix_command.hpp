/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#ifndef MATRIX_COMMAND_HPP
#define MATRIX_COMMAND_HPP
#include <LiquidCrystal.h>
#include "lcd_framebuffer.hpp"

void matrix_command_handler(LiquidCrystal&, lcd_framebuffer&);


void cmd_change_baud_rate();
void cmd_set_cursor(lcd_framebuffer&);
void cmd_create_character(LiquidCrystal&);
void cmd_save_custom_characters();
void cmd_load_custom_characters(LiquidCrystal&);
void cmd_save_startup_custom_characters();
void cmd_initialize_medium_numbers(LiquidCrystal&);
void cmd_place_medium_number(LiquidCrystal&);
void cmd_initialize_horizontal_bar(LiquidCrystal&);
void cmd_place_horizontal_bar_graph(LiquidCrystal&);
void cmd_initialize_vertical_bar(LiquidCrystal&);
void cmd_place_vertical_bar(LiquidCrystal&);
void cmd_dallas_ow();
void cmd_backlight_on();
void cmd_backlight_off();
void cmd_set_brightness();
void cmd_set_brightness_and_save();
void cmd_write_customer_data();
void cmd_read_customer_data();
void cmd_undocumented_dump_eeprom();
void cmd_undocumented_upload_eeprom();

#endif
/*
SPDX-License-Identifier: CC0-1.0
*/
#ifndef LCD_IDENTITY_HPP
#define LCD_IDENTITY_HPP
#include "Configuration.h"
// This file, LCD_Identity.hpp, is explicitly released in the public domain (CC0 to be specific <https://creativecommons.org/public-domain/cc0/>)
// It's mostly data collected from multiple datasheets. May it be of use to you.

constexpr unsigned char Get_MO_ModelId() {
    if (CONF_LCD_ROWS == 2) {
        if (CONF_LCD_COLS == 8) {
            return 0x01;
        } else if (CONF_LCD_COLS == 16) {
            return 0x34;
        } else if (CONF_LCD_COLS == 20) {
            return 0x08;
        } else if (CONF_LCD_COLS == 24) {
            return 0x4F; // MOS-AL242A
        } else if (CONF_LCD_COLS == 40) {
            return 0x4B;
        }
    } else if (CONF_LCD_ROWS == 4) {
        if (CONF_LCD_COLS == 20) {
            return 0x09;
        } else if (CONF_LCD_COLS == 40) {
            return 0x73;
        }
    }
    return 0x00;
}
static_assert(Get_MO_ModelId() != 0x00, "LCD configuration doesn't match any known MO displays, thus we cannot emulate one!");

constexpr const char* Get_MO_ModelName() {
    // 0x01 - 0x35 taken from the PCB Revision 2.2 manual for LK162-12
    // Also filled with the table from the PCB Revision 2.0 manual for LK402-25
    switch (Get_MO_ModelId()) {
        case 0x01:
            return "LCD0821";
        case 0x02: // Yes, LCD2021 is listed with two IDs.
        case 0x03:
            return "LCD2021";
        case 0x04:
            return "LCD1641";
        case 0x05:
            return "LCD2041";
        case 0x06:
            return "LCD4021";
        case 0x07:
            return "LCD4041";
        case 0x08:
            return "LK202-25";
        case 0x09:
            return "LK204-25";
        case 0x0A:
            return "LK404-55";
        case 0x0B:
            return "VFD2021";
        case 0x0C:
            return "VFD2041";
        case 0x0D:
            return "VFD4021";
        case 0x0E:
            return "VK202-25";
        case 0x0F:
            return "VK204-25";
        case 0x10:
            return "GLC12232";
        case 0x11:
            return "GLC12864";
        case 0x12:
            return "GLC128128";
        case 0x13:
            return "GLC24064";
        case 0x14:
            return "GLK12864-25";
        case 0x15:
            return "GLK24064-25";
        case 0x21:
            return "GLK128128-25";
        case 0x22:
            return "GLK12232-25";
        case 0x24:
            return "GLK12232-25-SM";
        case 0x25:
            return "GLK24064-16-1U-USB";
        case 0x26:
            return "GLK24064-16-1U";
        case 0x27:
            return "GLK19264-7T-1U-USB";
        case 0x28:
            return "GLK12232-16";
        case 0x29:
            return "GLK12232-16-SM";
        case 0x2A:
            return "GLK19264-7T-1U";
        case 0x2B:
            return "LK204-7T-1U";
        case 0x2C:
            return "LK204-7T-1U-USB";
        case 0x31:
            return "LK404-AT";
        case 0x32:
            return "VFD1621/MOS-AV-162A";
        case 0x33:
            return "LK402-12";
        case 0x34:
            return "LK162-12";
        case 0x35:
            return "LK204-25PC";
        case 0x36:
            return "LK202-24-USB";
        case 0x37:
            return "VK202-24-USB";
        case 0x38:
            return "LK204-24-USB";
        case 0x39:
            return "VK204-24-USB";
        case 0x3A:
            return "PK162-12";
        case 0x3B:
            return "VK162-12";
        case 0x3C:
            return "MOS-AP-162A";
        case 0x3D:
            return "PK202-25";
        case 0x3E:
            return "MOS-AL-162A";
        case 0x3F:
            return "MOS-AL-202A";
        case 0x40:
            return "LK162-12-USB/MOS-AV-202A";
        case 0x41:
            return "MOS-AP-202A";
        case 0x42:
            return "PK202-24-USB";
        case 0x43:
            return "MOS-AL-082";
        case 0x44:
            return "MOS-AL-204";
        case 0x45:
            return "MOS-AV-204";
        case 0x46:
            return "MOS-AL-402";
        case 0x47:
            return "MOS-AV-402";
        case 0x48:
            return "LK082-12";
        case 0x49:
            return "VK402-12";
        case 0x4A:
            return "VK404-55";
        case 0x4B:
            return "LK402-25";
        case 0x4C:
            return "VK402-25";
        case 0x4D:
            return "PK204-25";
        case 0x4F:
            return "MOS";
        case 0x50:
            return "MOI";
        case 0x51:
            return "XBoard-S";
        case 0x52:
            return "XBoard-I";
        case 0x53:
            return "MOU";
        case 0x54:
            return "XBoard-U";
        case 0x55:
            return "LK202-25-USB";
        case 0x56:
            return "VK202-25-USB";
        case 0x57:
            return "LK204-25-USB";
        case 0x58:
            return "VK204-25-USB";
        case 0x5B:
            return "LK162-12-TC";
        case 0x72:
            return "GLK240128-25";
        case 0x73:
            return "LK404-25";
        case 0x74:
            return "VK404-25";
        case 0x78:
            return "GLT320240";
        case 0x79:
            return "GLT480282";
        case 0x7A:
            return "GLT240128";
        
        default:
            return "Unknown";
    }
}

#endif
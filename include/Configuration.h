/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#ifndef CONFIGURATION_H
#define CONFIGURATION_H

// LCD configuration. To emulate an MO display, this must match one of them - 2x8/16/20/40, or 4x20/40.
const char CONF_LCD_ROWS = 2;
const char CONF_LCD_COLS = 40;

// Pin configuration.
// 8-bit bus is pointless since we can drive the data at 2 MHz, which is more than enough for serial to be a bottle neck.
const char CONF_LCD_RS = 8;
const char CONF_LCD_E = 9;
const char CONF_LCD_D4 = 4;
const char CONF_LCD_D5 = 5;
const char CONF_LCD_D6 = 6;
const char CONF_LCD_D7 = 7;

#endif
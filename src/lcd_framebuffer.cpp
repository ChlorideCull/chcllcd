/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#include "lcd_framebuffer.hpp"

lcd_framebuffer::lcd_framebuffer(LiquidCrystal& lcd, uint8_t columns, uint8_t rows) {
    this->lcd = &lcd;
    this->columns = columns;
    this->rows = rows;
    memset(this->framebuffer, 0x20, sizeof(this->framebuffer));
}

lcd_framebuffer::~lcd_framebuffer() {
}

void lcd_framebuffer::clear() {
    memset(this->framebuffer, 0x20, sizeof(this->framebuffer));
    this->lcd->clear();
    this->setCursor(0, 0);
}

void lcd_framebuffer::sync() {
    lcd->noAutoscroll();
    lcd->clear();
    for (size_t y = 0; y < this->rows; y++)
    {
        lcd->setCursor(0, y);
        for (size_t x = 0; x < this->columns; x++)
        {
            lcd->write(this->framebuffer[y][x]);
        }
    }
    lcd->setCursor(this->cursor_col, this->cursor_row);
}

void lcd_framebuffer::setCursor(uint8_t col, uint8_t row) {
    this->cursor_col = col;
    this->cursor_row = row;
    this->lcd->setCursor(col, row);
}

void lcd_framebuffer::write(const char* data) {
    auto len = strlen(data);
    for (size_t i = 0; i < len; i++)
    {
        this->write(data[i]);
    }
}

void lcd_framebuffer::write(uint8_t data) {
    // TODO: Figure out the exact logic of autoscroll and linewrap flags
    bool cursorForceMoved = false;

    if (this->cursor_row >= this->rows) {
        if (this->autoScroll) {
            this->scroll();
        }
        this->cursor_row = this->cursor_row % this->rows;
        this->lcd->setCursor(this->cursor_col, this->cursor_row);
    }

    if (data == '\n') {
        this->cursor_col = 0;
        this->cursor_row += 1;
        cursorForceMoved = true;
        if (this->autoScroll && this->cursor_row >= this->rows) {
            this->scroll();
        }
    } else {
        framebuffer[this->cursor_row][this->cursor_col] = data;
        this->lcd->write(data);
        this->cursor_col += 1;
    }

    if (this->lineWrap && this->cursor_col >= this->columns) {
        this->cursor_row += 1;
        this->cursor_col = 0;
        cursorForceMoved = true;
    }

    if (cursorForceMoved) {
        this->lcd->setCursor(this->cursor_col, this->cursor_row);
    }
}

void lcd_framebuffer::scroll() {

}
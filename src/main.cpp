/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#include <Arduino.h>
#include <LiquidCrystal.h>
#include "Configuration.h"
#include "LCD_Identity.hpp"
#include "matrix_command.hpp"
#include "lcd_framebuffer.hpp"
#include "persistent_settings.hpp"

LiquidCrystal lcd(CONF_LCD_RS, CONF_LCD_E, CONF_LCD_D4, CONF_LCD_D5, CONF_LCD_D6, CONF_LCD_D7);
lcd_framebuffer fb(lcd, CONF_LCD_COLS, CONF_LCD_ROWS);

void setup() {
    delay(50);
    lcd.begin(CONF_LCD_COLS, CONF_LCD_ROWS);
    fb.sync();
    persistent_settings ps;
    get_persistent_settings(ps);
    for (size_t i = 0; i < 8; i++)
    {
        lcd.createChar(i, ps.customCharacterBanks[BANKID_STARTUP][i]);
    }
    
    for (size_t y = 0; y < CONF_LCD_ROWS; y++)
    {
        fb.setCursor(0, y);
        for (size_t x = 0; x < CONF_LCD_COLS; x++)
        {
            fb.write(ps.bootScreen[y][x]);
        }
    }
    Serial.begin(19200);
}

void loop() {
    if (Serial.available() > 0) {
        uint8_t val = Serial.read();
        if (val != 0xFE) {
            fb.write(val);
        } else {
            matrix_command_handler(lcd, fb);
        }
    }
}
/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#include "matrix_command.hpp"
#include "LCD_Identity.hpp"
#include "util.h"
#include "persistent_settings.hpp"
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <EEPROM.h>

void matrix_command_handler(LiquidCrystal& lcd, lcd_framebuffer &fb) {
    auto data = blockingSerialRead();
    switch (data) {
        case 0x39:
            cmd_change_baud_rate();
            break;

        case 0xCA: // 8.2 Set Data Lock
        case 0xCB: // 8.3 Set and Save Data Lock
            // Deliberately not implemented, with three parameters
            blockingSerialRead();
            blockingSerialRead();
        case 0x55: // 6.5 Set Debounce Time
        case 0x7E: // 6.6 Set Auto Repeat Mode
            // Deliberately not implemented, with one parameter
            blockingSerialRead();
        case 0x33: // 1.2 Change I2C Slave Address
        case 0xA0: // 1.3 Transmission Protocol Select
        case 0xA4: // 1.4 Set a Non-Standard Baud Rate
        case 0x40: // 2.2 Change the Start Up Screen
        case 0x41: // 6.1 Auto Transmit Key Presses On
        case 0x4F: // 6.2 Auto Transmit Key Presses Off
        case 0x26: // 6.3 Poll Key Press
        case 0x45: // 6.4 Clear Key Buffer
        case 0x60: // 6.7 Auto Repeat Mode Off
            // Deliberately not implemented
            break;

        case 0x58:
            fb.clear();
            break;
        
        case 0x51:
            // 2.3 Auto Scroll On
            fb.autoScroll = true;
            break;

        case 0x52:
            // 2.4 Auto Scroll Off
            fb.autoScroll = false;
            break;
        
        case 0x43:
            // 2.5 Set Auto Line Wrap On
            fb.lineWrap = true;
            break;
        
        case 0x44:
            // 2.6 Set Auto Line Wrap Off
            fb.lineWrap = false;
            break;
        
        case 0x47:
            cmd_set_cursor(fb);
            break;
        
        case 0x48:
            lcd.home();
            break;

        case 0x4C:
            // TODO: 2.9 Move Cursor Back
            break;
        
        case 0x4D:
            // TODO: 2.10 Move Cursor Forward
            break;
        
        case 0x4A:
            // TODO: 2.11 Underline Cursor On
            break;
        
        case 0x4B:
            // TODO: 2.12 Underline Cursor Off
            break;
        
        case 0x53:
            // TODO: 2.13 Blinking Block Cursor On
            break;

        case 0x54:
            // TODO: 2.14 Blinking Block Cursor Off
            break;
        
        case 0x4E:
            cmd_create_character(lcd);
            break;

        case 0xC1:
            cmd_save_custom_characters();
            break;
        
        case 0xC0:
            cmd_load_custom_characters(lcd);
            break;
        
        case 0xC2:
            cmd_save_startup_custom_characters();
            break;
        
        case 0x6D:
            cmd_initialize_medium_numbers(lcd);
            break;
        
        case 0x6F:
            cmd_place_medium_number(lcd);
            break;
        
        case 0x68:
            cmd_initialize_horizontal_bar(lcd);
            break;

        case 0x7C:
            cmd_place_horizontal_bar_graph(lcd);
            break;
        
        case 0x73:
            // TODO: 3.9 Initialize Narrow Vertical Bar
            break;
        
        case 0x76:
            cmd_initialize_vertical_bar(lcd);
            break;

        case 0x3D:
            cmd_place_vertical_bar(lcd);
            break;

        case 0x57:
        case 0x56:
        case 0x25:
            // 7.4 GPO, one parameter
            blockingSerialRead();
            break;
        case 0xC3:
            // 7.4 GPO, two parameters
            blockingSerialRead();
            blockingSerialRead();
            break;
        
        case 0xC8:
            cmd_dallas_ow();
            break;

        case 0x42:
            cmd_backlight_on();
            break;
        
        case 0x46:
            cmd_backlight_off();
            break;

        case 0x99:
            cmd_set_brightness();
            break;
        
        case 0x98:
            cmd_set_brightness_and_save();
            break;
        
        case 0x82:
            // TODO: 7.5 Set Backlight Colour
            break;
        
        case 0x50:
            // TODO: 7.6 Set Contrast
            break;
        
        case 0x91:
            // TODO: 7.7 Set and Save Contrast
            break;
        
        case 0x93:
            // TODO: 8.1 Set Remember
            break;

        case 0x34:
            cmd_write_customer_data();
            break;

        case 0x35:
            cmd_read_customer_data();
            break;
        
        case 0x36:
            Serial.write((uint8_t)0x51);
            break;
        
        case 0x37:
            Serial.write(Get_MO_ModelId());
            break;
        
        case 0xD0:
            cmd_undocumented_dump_eeprom();
            break;
        
        case 0x90:
            cmd_undocumented_upload_eeprom();
            break;

        default:
            break;
    }
}

uint32_t mapbaudrate(const uint8_t val) {
    switch (val) {
        case 83:
            return 1200;
        case 41:
            return 2400;
        case 207:
            return 4800;
        case 103:
            return 9600;
        case 51:
        default:
            return 19200;
        case 34:
            return 28800;
        case 25:
            return 38400;
        case 16:
            return 57600;
        case 12:
            return 76800;
        case 8:
            return 115200;
    }
}

void cmd_change_baud_rate() {
    // 1.1 Change Baud Rate
    auto val = blockingSerialRead();
    auto newRate = mapbaudrate(val);
    persistent_settings settings;
    get_persistent_settings(settings);
    settings.baudRate = newRate;
    set_persistent_settings(settings);
    Serial.end();
    Serial.begin(newRate);
}

void cmd_set_cursor(lcd_framebuffer& fb) {
    auto column = blockingSerialRead();
    auto row = blockingSerialRead();
    fb.setCursor(column-1, row-1);
}

void cmd_create_character(LiquidCrystal& lcd) {
    auto id = blockingSerialRead();
    uint8_t data[8];
    Serial.readBytes(data, 8);
    lcd.createChar(id, data);
}

void cmd_save_custom_characters() {
    // 3.2 Save Custom Characters
    auto bank = blockingSerialRead();
    auto id = blockingSerialRead();
    uint8_t data[8];
    Serial.readBytes(data, 8);

    persistent_settings settings;
    get_persistent_settings(settings);
    memcpy(settings.customCharacterBanks[bank][id], data, 8);
    set_persistent_settings(settings);
}

void cmd_load_custom_characters(LiquidCrystal& lcd) {
    // 3.3 Load Custom Characters
    auto bank = blockingSerialRead();
    persistent_settings settings;
    get_persistent_settings(settings);
    for (size_t i = 0; i < 8; i++)
    {
        lcd.createChar(i, settings.customCharacterBanks[bank][i]);
    }
}

void cmd_save_startup_custom_characters() {
    // TODO: 3.4 Save Start Up Screen Custom Characters
    auto id = blockingSerialRead();
    uint8_t data[8];
    Serial.readBytes(data, 8);

    persistent_settings settings;
    get_persistent_settings(settings);
    memcpy(settings.customCharacterBanks[BANKID_STARTUP][id], data, 8);
    set_persistent_settings(settings);
}

void cmd_initialize_medium_numbers(LiquidCrystal& lcd) {
    // 3.5 Initialize Medium Numbers
    persistent_settings settings;
    get_persistent_settings(settings);
    for (size_t i = 0; i < 8; i++)
    {
        lcd.createChar(i, settings.customCharacterBanks[BANKID_MEDIUM_DIGITS][i]);
    }
}

void cmd_place_medium_number(LiquidCrystal& lcd) {
    // TODO: 3.6 Place Medium Numbers
    auto row = blockingSerialRead();
    auto column = blockingSerialRead();
    auto digit = blockingSerialRead();
}

void cmd_initialize_horizontal_bar(LiquidCrystal& lcd) {
    // 3.7 Initialize Horizontal Bar
    persistent_settings settings;
    get_persistent_settings(settings);
    for (size_t i = 0; i < 8; i++)
    {
        lcd.createChar(i, settings.customCharacterBanks[BANKID_HORIZONTAL_BARS][i]);
    }
}

void cmd_place_horizontal_bar_graph(LiquidCrystal& lcd) {
    // TODO: 3.8 Place Horizontal Bar Graph
    auto column = blockingSerialRead();
    auto row = blockingSerialRead();
    auto direction = blockingSerialRead();
    auto length = blockingSerialRead();
}

void cmd_initialize_vertical_bar(LiquidCrystal& lcd) {
    // 3.10 Initialize Wide Vertical Bar
    persistent_settings settings;
    get_persistent_settings(settings);
    for (size_t i = 0; i < 8; i++)
    {
        lcd.createChar(i, settings.customCharacterBanks[BANKID_VERTICAL_BARS][i]);
    }
}

void cmd_place_vertical_bar(LiquidCrystal& lcd) {
    // TODO: 3.11 Place Vertical Bar
    auto column = blockingSerialRead();
    auto length = blockingSerialRead();
}

void cmd_dallas_ow() {
    auto cmd = blockingSerialRead();
    if (cmd == 0x02) {
        // Pretend there's an empty bus, so don't send an ID packet
        return;
    } else if (cmd == 0x01) {
        // Read expected data, and just respond "no device found".
        blockingSerialRead();
        blockingSerialRead();
        blockingSerialRead();
        char scratch[16];
        while (Serial.readBytes(scratch, 16) == 16) { }
        Serial.write('2');
    } else {
        Serial.write('1');
    }
}

void cmd_backlight_on() {
    // TODO: 7.1 Backlight On
}

void cmd_backlight_off() {
    // TODO: 7.2 Backlight Off
}

void cmd_set_brightness() {
    // TODO: 7.3 Set Brightness
    blockingSerialRead();
}

void cmd_set_brightness_and_save() {
    // TODO: 7.4 Set and Save Brightness
    blockingSerialRead();
}

void cmd_read_customer_data() {
    for (size_t i = 0; i < 16; i++)
    {
        Serial.write(EEPROM.read(i));
    }
}

void cmd_write_customer_data() {
    uint8_t block[16];
    Serial.readBytes(block, 16);
    for (size_t i = 0; i < 16; i++)
    {
        EEPROM.update(i, block[i]);
    }
}

void cmd_undocumented_dump_eeprom() {
    // Undocumented command to dump the EEPROM.
    persistent_settings settings;
    get_persistent_settings(settings);
    Serial.write(reinterpret_cast<uint8_t*>(&settings), sizeof(persistent_settings));
}

void cmd_undocumented_upload_eeprom() {
    // Undocumented command to upload the EEPROM.
    persistent_settings settings;
    if (Serial.readBytes(reinterpret_cast<uint8_t*>(&settings), sizeof(persistent_settings)) != sizeof(persistent_settings)) {
        return;
    }
    set_persistent_settings(settings);
}
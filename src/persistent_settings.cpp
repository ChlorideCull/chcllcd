/*
SPDX-FileCopyrightText: 2024 Sebastian "Dusty the Fuzzy Dragon" Johansson
SPDX-License-Identifier: CERN-OHL-S-2.0+
*/
#include "persistent_settings.hpp"
#include <EEPROM.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "LCD_Identity.hpp"

const uint8_t defaultCharacterBanks[5][8][8] PROGMEM = {
    { // Startup
        {
            0b00000,
            0b00000,
            0b00000,
            0b00001,
            0b00111,
            0b01001,
            0b00011,
            0b00111
        }, { 
            0b00000,
            0b10000,
            0b01100,
            0b11111,
            0b10011,
            0b01111,
            0b11111,
            0b11100
        }, {
            0b00000,
            0b00000,
            0b00000,
            0b11000,
            0b00100,
            0b10000,
            0b11100,
            0b11011
        }, {
            0b01101,
            0b00001,
            0b00110,
            0b00100,
            0b00100,
            0b00011,
            0b00000,
            0b00000
        }, { 
            0b10011,
            0b00010,
            0b00011,
            0b00000,
            0b10000,
            0b00000,
            0b10000,
            0b01000
        }, { 
            0b01100,
            0b01010,
            0b01010,
            0b01000,
            0b01000,
            0b10000,
            0b10000,
            0b10000
        }, { 
            0b00000,
            0b01010,
            0b01010,
            0b00000,
            0b10001,
            0b10101,
            0b01010,
            0b00000
        },
        { 0, 0, 0, 0, 0, 0, 0, 0 }
    },
    { // Horizontal
        { 16, 16, 16, 16, 16, 16, 16, 16 },
        { 24, 24, 24, 24, 24, 24, 24, 24 },
        { 28, 28, 28, 28, 28, 28, 28, 28 },
        { 30, 30, 30, 30, 30, 30, 30, 30 },
        { 1, 1, 1, 1, 1, 1, 1, 1 },
        { 3, 3, 3, 3, 3, 3, 3, 3 },
        { 7, 7, 7, 7, 7, 7, 7, 7 },
        { 15, 15, 15, 15, 15, 15, 15, 15 }
    },
    { // Vertical
        { 31, 31, 31, 31, 31, 31, 31, 31 },
        { 0, 0, 0, 0, 0, 0, 0, 31 },
        { 0, 0, 0, 0, 0, 0, 31, 31 },
        { 0, 0, 0, 0, 0, 31, 31, 31 },
        { 0, 0, 0, 0, 31, 31, 31, 31 },
        { 0, 0, 0, 31, 31, 31, 31, 31 },
        { 0, 0, 31, 31, 31, 31, 31, 31 },
        { 0, 31, 31, 31, 31, 31, 31, 31 }
    },
    { // Medium Digits
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 }
    },
    { // Large Digits
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 0, 0, 0, 0 }
    }
};

void set_default_bootscreen(persistent_settings &settings) {
    memset(settings.bootScreen, 0x20, sizeof(settings.bootScreen));
    const char* header = "chclLCD\6";
    const auto offset = 4;
    if (CONF_LCD_COLS < strlen(header) + offset) {
        // Screen too small - put the bare minimum as default
        memcpy(settings.bootScreen[0], header, strlen(header));
        memcpy(settings.bootScreen[CONF_LCD_ROWS-1], Get_MO_ModelName(), strlen(Get_MO_ModelName()));
    } else {
        const char* prefix = "V:1 E:";
        const auto maxLineLength = offset + strlen(header) > offset + strlen(prefix) + strlen(Get_MO_ModelName()) ?
            offset + strlen(header) :
            offset + strlen(prefix) + strlen(Get_MO_ModelName());
        const uint32_t padding = (CONF_LCD_COLS - maxLineLength) / 2.0f;
        
        const auto screenCenter = CONF_LCD_ROWS/2;
        memcpy(settings.bootScreen[screenCenter-1]+offset+padding, header, strlen(header));
        memcpy(settings.bootScreen[screenCenter]+offset+padding, prefix, strlen(prefix));
        memcpy(settings.bootScreen[screenCenter]+offset+padding+strlen(prefix), Get_MO_ModelName(), strlen(Get_MO_ModelName()));
        memcpy(settings.bootScreen[screenCenter-1]+padding, "\0\1\2", 3);
        memcpy(settings.bootScreen[screenCenter]+padding, "\3\4\5", 3);
    }
}

void clear_persistent_settings() {
    persistent_settings new_settings;
    new_settings.version = 1;
    new_settings.identity = Get_MO_ModelId();
    new_settings.baudRate = 19200;

    set_default_bootscreen(new_settings);
    
    memcpy_P(new_settings.customCharacterBanks, defaultCharacterBanks, sizeof(new_settings.customCharacterBanks));

    EEPROM.put(0, new_settings);
}

void get_persistent_settings(persistent_settings& out) {
    EEPROM.get(0, out);
    if (out.version != 1 || out.identity != Get_MO_ModelId()) {
        clear_persistent_settings();
        get_persistent_settings(out);
    }
}

void set_persistent_settings(const persistent_settings& settings) {
    EEPROM.put(0, settings);
}